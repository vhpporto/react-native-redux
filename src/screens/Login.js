import React, { useState } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, TextInput, Alert, ActivityIndicator, KeyboardAvoidingView } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { LinearGradient  } from 'expo-linear-gradient'
import md5 from 'md5'
import * as Animatable from 'react-native-animatable'


const Login = ( { navigation } ) => {
    const [ id, setId ] = useState('')
    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')
    const [ loading, setLoading ] = useState(false)
    const dispatch = useDispatch()

    

    const addCourse = (id) => { 
        dispatch({ type: 'ON_LOGGIN', user: id })
        navigation.navigate('Home')
    }

    _onLogin = async () => {
        setLoading(true)
        const API_URL = 'https://api.appbarber.com.br/loginv2'
        const response = await axios.post(API_URL, {
            api_key: 'TcskrU3Ejze.6',
            email: email,
            senha: md5(password),
            pescodigo: '882438'
        })

        const { result } = response.data
        const [ { resultado, erro, id  } ] = result
        setId(id)
        setLoading(false)
        
        erro == 0
            ? addCourse(id)       
            : Alert.alert('Oops..',`${resultado}`)

    }


    return (
        <KeyboardAvoidingView style={{flex: 1}} behavior='padding' enabled>
        <LinearGradient
            colors={["#f4511e", "#7c2362"]}
            style={{ flex: 1 }}
            start={{ x: 1, y: 0 }}
            end={{ x: 0, y: 1 }}>

                {loading && <ActivityIndicator style={styles.loading} />}

                <Animatable.View style={styles.container}
                    animation='bounceInLeft'
                    useNativeDriver>
                        <TextInput placeholder='Email'
                            style={styles.input}
                            color='#fff'
                            placeholderTextColor='#FFF'
                            value={email}
                            onChangeText={text => setEmail(text)}
                            />

                        <TextInput placeholder='Nome'
                            style={styles.input}
                            color='#fff'
                            placeholderTextColor='#FFF'
                            value={password}
                            onChangeText={text => setPassword(text)}
                            />
                        
                        <TouchableOpacity style={styles.btAccess}
                            onPress={_onLogin}>
                            <Text style={styles.textButton}>ACESSAR</Text>
                        </TouchableOpacity>

                </Animatable.View>
            </LinearGradient>
            </KeyboardAvoidingView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        height: 40,
        width: '90%',
        paddingLeft: 20,
        borderRadius: 4,
        borderBottomWidth: 1,
        marginBottom: 10,
        borderBottomColor: '#D9D9D9'
    },
    btAccess: {
        height: 40,
        width: '90%',
        borderRadius: 4,
        backgroundColor: '#008fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textButton: {
        color: '#FFF',
        fontWeight: 'bold',
    },
    loading: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center'
    }

})


export default Login
