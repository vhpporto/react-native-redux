import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { LinearGradient } from 'expo-linear-gradient'
import { Ionicons } from '@expo/vector-icons'
import axios from 'axios'






const Home = () => {
    const [ data, setData ] = useState('')
    const dispatch = useDispatch()
    const id = useSelector(state => state.email)

    useEffect(() => {
        // console.log(id)
        _buscaPerfil()
    }, [])

    // function addCourse(imagem) { 
    //     dispatch({ type: 'ON_LOGGIN', image: imagem })
    // }
 

    const _buscaPerfil = async () => {
        
        const api_key = 'TcskrU3Ejze.6'
        const response = await axios.get(`https://api.appbarber.com.br/perfil?api_key=${api_key}&id=${id}`)
    
        const { result } = response.data
        setData(result)
        console.log(result)

        // addCourse(result)



    }

    return (
        <View style={styles.container}>

            {id.map(id => <Text>{id}</Text>)}
            
        </View>
    )
}


Home.navigationOptions = ({ navigation }) => ({
    title: "Home",
    headerTitleStyle: {
        textAlign: "left",
        fontSize: 24
    },
    headerTintColor: "rgba(255,255,255,0.8)",
    headerBackground: (
        <LinearGradient
            colors={["#f4511e", "#7c2362"]}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 1 }}
            style={{ flex: 1 }}
        />
    ),
    headerRightContainerStyle: {
        paddingRight: 20
    },
    headerRight: (
        <>
        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
            <Image style={styles.userImage} source={{uri: "https://s3-sa-east-1.amazonaws.com/img-appbarber-appbeleza/babervp/5e3c0e793cc9e.png"}}/>
           
        </TouchableOpacity>
        </>
    )
})



const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    userImage: {
        alignSelf: 'center',
        height: 40,
        width: 40,
        borderRadius: 40/2,
        borderWidth: 0.5
    }
})

export default Home
