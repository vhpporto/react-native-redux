import { createStore } from 'redux'


const INITIAL_STATE = {
    email: null,
    id: null,
}

function courses(state = INITIAL_STATE, action) {
    switch (action.type) {
        case "ON_LOGGIN": 
            return {
                ...state,
                name: action.user.email,
                id: action.user.id,
            }
        default:
            return state
    }
}

const store = createStore(courses)


export default store