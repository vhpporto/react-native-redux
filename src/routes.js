import React from 'react'
import { createAppContainer, createSwitchNavigator,   } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { LinearGradient } from 'expo-linear-gradient'

import Login from './screens/Login'
import Home from './screens/Home'

const StackNavigator = createStackNavigator({
    Home: {
        screen: Home,
        // navigationOptions: {
        //     headerTintColor: '#FFF',
        //     headerBackground: (
        //         <LinearGradient
        //           colors={["#f4511e", "#7c2362"]}
        //           style={{ flex: 1 }}
        //           start={{ x: 0, y: 0 }}
        //           end={{ x: 1, y: 0 }}
        //         />
        //         ),
            
        // }

    }
})

const SwitchNavigator = createSwitchNavigator({
    Login: Login,
    Home: StackNavigator

},
{
    initialRouteName: 'Login'
})

const AppNavigator = createAppContainer(SwitchNavigator)

export default AppNavigator